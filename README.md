# usbip-tools

Collection of scripts to manage connection of USB devices over the network

## Usage
On the host add a `devices.txt` file that contains
the `vendor:device` ID pairs to expose.
```
$ cat devices.txt
Bus 001 Device 003: ID d34d:b33f Thing you want to connect
```
Then start the server using `./usbip-server.sh run`.

The server will log all found local device numbers.
You will need them on the client. The numbers look like this: `1-1.3`.

On the client, attach the device using the host IP the remote device number.
```
$ sudo usbip attach -r 192.168.178.4 -b 1-1.3
```

Then use it as a normal USB device.
```
$ sudo fdisk -l /dev/sda
Disk /dev/sda: 14.92 GiB, 16021192704 bytes, 31291392 sectors
Disk model: Card  Reader
...
```

Detach the the device using `sudo usbip detach -p 00`.


## Test Setups

### **OK**: Connecting an SD Card Reader from one RPi4 to another RPi4

**Client**: `usbip` on RPi4

**Host**: `usbipd` on RPi4 with card reader + SD card

```
sudo usbip attach -r 192.168.178.4 -b 1-1.3
sudo fdisk -l /dev/sda
sudo usbip detach -p 00
```
✅ fdisk showed the partition on the SD card
stability seemed to be good. disk mounting not tested yet.

### **Fail**: Connect via usbip-win to RPi4

**Client**:  https://github.com/cezanne/usbip-win/releases, Windows 11

**Host**: `usbipd` on RPi4 with card reader + SD card

Using the windows VHCI driver to connect to card reader on RPi4 did not work.
The device was visible but when mounted (accessing the files) there were garbage
files listed in the explorer.
