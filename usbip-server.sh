#!/usr/bin/env bash

REGEX_USBID="[0-9a-f]{4}:[0-9a-f]{4}"
REGEX_USBNUM="[0-9]-[0-9]\\.[0-9]"

device_ids=""
device_numbers=""
local_devices=""

usage() {
	echo "usage: $0 [run|start|stop|restart|bind|unbind|watch|ids]"
}

omit_comments() { grep -v '^#' < "$1"; }

check_module() {
	echo starting USB server
	if modprobe usbip_host 2> /dev/null
	then echo found usbip_host module
	else echo WARNING: usbip_host module not detected
	fi
}

load_module() {
	echo "loading usbip_host module"
	sudo modprobe usbip_host
}

stop_server() {
	echo "stopping server"
	sudo killall usbipd 2> /dev/null
}

start_server() {
	load_module
	echo "looking for running server"
	pid=$(pgrep usbipd)
	if test -n "$pid"; then
		echo "server already running pid=$pid"
		return
	fi
	echo "starting server (in background)"
	sudo usbipd -D
	echo "started server"
}

watch_server() {
	# forward logs
	dmesg -w | grep -o -E "usbip(-host)?.*"
}

watch_devices() {
	echo "start watching for usb device reconnections"
	udevadm monitor -u | grep -q -m 1 -E "UDEV .* (add|bind|unbind|remove|change)"
	echo "usb device changes detected"
}

load_config() {
	echo exposing target devides:
	cat devices.txt
	device_ids=$(omit_comments devices.txt | grep -E -o "$REGEX_USBID")
}

load_devices() {
	load_config

	local_devices=$(usbip list -p -l)
	echo loaded local devices "$local_devices"

	device_numbers=""
	echo looking target device IDs in local devices
	for id in $device_ids; do
		line=$(echo "$local_devices" | grep "$id")
		num=$(echo "$line" | grep -E -o "$REGEX_USBNUM")

		if test -n "$num"; then
			echo found device ID for local devide id="$id" num="$num"
			device_numbers="$device_numbers $num"
		fi
	done
}

bind_or_unbind_devices() {
	load_devices

	cmd="$1"
	verbing="${1}ing"
	test -n "$cmd" || return 1

	if test -n "$device_numbers"; then
		check_module
		echo "$verbing devices: $device_numbers"
		for dev in $device_numbers; do
			echo "$verbing" device: "$dev"
			sudo usbip "$cmd" -b "$dev"
		done
	fi
}

bind_devices()   { bind_or_unbind_devices bind;   }
unbind_devices() { bind_or_unbind_devices unbind; }

cleanp() {
	# add all programs here that will run in the background
	killall dmesg
}

run() {
	start_server
	echo "initally binding devices"
	bind_devices
	echo "starting to watch for usb device changes to trigger rebind"
	while watch_devices; do
		echo "rebind needed, waiting 2s for local device bindings to settle"
		sleep 2
		bind_devices
	done
}

trap "cleanup 2> /dev/null" EXIT

if test $# -eq 0
then usage; exit 1
else echo running commands: "$@"
fi

for i in "$@"; do case $i in
	re[start]*) stop_server; start_server;;
	ru[n]*)     run;;
	sta[rt]*)   start_server;;
	sto[p]*)    stop_server;;
	b[ind]*)    bind_devices;;
	u[nbind]*)  unbind_devices;;
	w[atch]*)   watch_server;;
	i[ds]*)     load_config; echo "device_ids: $device_ids";;
	*)          usage && exit 1;;
esac; done

# vim: ts=4:sw=4
